﻿using System;

namespace PRLogic2
{
    class Program
    {
        static void Main(string[] args)
        {
            bool ulangi = true;
            while (ulangi)
            {
                Console.WriteLine("LOGIC DAY 2 - Materi :");
                Console.WriteLine("a. For");
                Console.WriteLine("b. For Bersarang");
                Console.WriteLine("c. Foreach");
                Console.Write("Pilih Soal (1-10): ");
                int pilih = int.Parse(Console.ReadLine());

                switch (pilih)
                {
                    case 1:
                        soal1();
                        break;
                    case 2:
                        soal2();
                        break;
                    case 3:
                        soal3();
                        break;
                    case 4:
                        soal4();
                        break;
                    case 5:
                        soal5();
                        break;
                    case 6:
                        soal6();
                        break;
                    case 7:
                        soal7();
                        break;
                    case 8:
                        soal8();
                        break;
                 
                    default:
                        break;
                }

                Console.WriteLine("Ulangi (y/n) = ");
                string yesno = Console.ReadLine();
                if (yesno.ToLower() == "n")
                {
                    ulangi = false;

                }
                Console.Clear();
            }
        }
        static void soal1()
        {

            Console.Write("Masukan Angka : ");
            int angka = int.Parse(Console.ReadLine());
            for (int i = 0; i <= angka; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.Write(" * ");
                }
                Console.WriteLine(" ");
            }
        }
        static void soal2()
        {
            Console.Write("Masukan Angka : ");
            int angka = int.Parse(Console.ReadLine());
            for (int i = 1; i <= angka; i++)
            {
                for (int j = 1; j <= angka; j++)
                {
                    Console.Write(" ");
                }
                for (int k = 1; k <= i; k++)
                {
                    Console.Write("*");
                }
                Console.WriteLine(" ");
            }
        }
        static void soal3()
        {
            Console.Write("Masukan Bilangan: ");
            int bil = int.Parse(Console.ReadLine());
            int x = 0;
            int y = 1;
            int z = 1;
            for (int i = 0; i < bil; i++)
            {
                Console.Write(z + ",");
                z = y;
                z = x + y;
                x = y;
                y = z;
            }
        }
        static void soal4()
        {
            Console.Write("Masukan Bilangan: ");
            int bil = int.Parse(Console.ReadLine());
            int a = 1;
            int b = -1;
            int c = 1;
            int total;

            for (int i = 0; i < bil; i++)
            {
                total = c;
                Console.Write(total + ",");
                total = a + b + c;
                a = b;
                b = c;
                c = total;
            }
        }
        static void soal5()
        {
            Console.Write("Masukan Batas Bilangan: ");
            int bil = int.Parse(Console.ReadLine());
            int temp = 0;

            for (int i = 1; i <= bil; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    if (i % j == 0)
                    {
                        temp += 1;
                    }
                }
                if (temp == 2)
                {
                    Console.Write(i + " ");
                }
                temp = 0;
            }
        }
        static void soal6()
        {
            Console.Write("Masukan n: ");
            int n = int.Parse(Console.ReadLine());

            while (n != 1)
            {
                if (n % 2 == 0)
                {
                    Console.WriteLine($"{n}/2={n / 2}");
                    n = n / 2;
                }
                else if (n % 3 == 0)
                {
                    Console.WriteLine($"{n}/3={n / 3}");
                    n = n / 3;
                }
                else if (n % 5 == 0)
                {
                    Console.WriteLine($"{n}/5={n / 5}");
                    n = n / 5;
                }
            }
        }
        static void soal7()
        {
            Console.Write("Input Bilangan :");
            string bil = Console.Read();
            string[] arrBil = bil.Split(",");
            int[] intbil = Array.ConvertAll(arrBil, int.Parse);

            Array.Sort(intbil);

            foreach (int value in intbil)
            {
                Console.Write($"{value} ");
            }
        }
        static void soal8()
        {
            Console.Write("Input N :");
            int n = int.Parse(Console.ReadLine());

            for (int i = 3; i <= n; i++)
            {
                if (n % 3 == 0)
                    Console.Write(n + " ");
            }
        }
    }
  }
