SELECT *FROM biodata
SELECT *FROM contact_person
SELECT *FROM department
SELECT *FROM employee
SELECT *FROM employee_leave
SELECT *FROM employee_position
SELECT *FROM family
SELECT *FROM leave
SELECT *FROM leave_request
SELECT *FROM position
SELECT *FROM travel_request
SELECT *FROM travel_settlement
SELECT *FROM travel_type

-- NOMER 1 TAMPILKAN DATA LENGKAP KARYAWAN DAN RATA-RATA GAJI SETAHUN UNTUK MASING2 DARI MEREKA
SELECT CONCAT (first_name, ' ', last_name) AS Fullname,
biodata.dob, (employee.salary/12)
FROM biodata 
LEFT JOIN employee ON biodata.id = employee.biodata_id



-- NOMER 2 TAMBAHKAN 3 ORANG PELAMAR 2 KARYWAN KONTRAK, 1 TIDAK DITERIMA
SELECT CONCAT(first_name, ' ', last_name) AS FULLNAME, NIP,
employee.status, employee.salary FROM biodata
LEFT JOIN employee ON biodata.id = employee.biodata_id

-- NOMER 3 Tampilkan fullname pelamar yang tanggal lahirnya antara 01-01-1991 s/d 31-12-1991
SELECT CONCAT(first_name, ' ', last_name) AS FULLNAME, dob FROM biodata
WHERE biodata.dob BETWEEN '01/01/1991' AND '31/12/1991'

-- NOMER 4 Tampilkan nama-nama pelamar yang tidak diterima sebagai karyawan
SELECT CONCAT(first_name, ' ', last_name) AS FULLNAME FROM biodata
LEFT JOIN employee ON biodata.id=employee.biodata_id
WHERE employee.status IS NULL

-- NOMER 5 Buat query cuti untuk bunga maria

-- NOMER 6 Tampilkan nama karyawan, jenis perjalanan dinas, tanggal perjalanan dinas, dan total pengeluarannya selama perjalanan dinas tersebut
SELECT CONCAT(first_name, ' ', last_name) AS Fullname, 


-- NOMER 7 Tampilkan sisa cuti tahun 2020 yang dimiliki oleh karyawan
SELECT 

-- NOMER 8 Urutkan nama-nama karyawan dan statusnya, diurutkan dari yang paling tua ke yang paling muda
SELECT CONCAT(first_name, ' ', last_name) AS Fullname,
employee.status, dob FROM biodata
LEFT JOIN employee ON biodata.id = employee.biodata_id
ORDER BY dob ASC

 -- NOMER 9 Tampilkan selisih antara total item cost dengan total travel fee untuk masing-masing karyawan
 SELECT CONCAT(first_name, ' ', last_name) AS Fullname, item_cost-travel_type.travel_fee AS Total_Travel
 FROM biodata
 LEFT JOIN employee ON employee.biodata_id = biodata.id
 LEFT JOIN travel_request ON travel_request.employee_id=employee.id
 LEFT JOIN travel_settlement ON travel_settlement.travel_request_id=travel_request.id
 LEFT JOIN travel_type ON travel_type.id=travel_settlement.travel_request_id

 -- NOMER 10 Tambahkan data cuti tahun 2021 terhadap semua karyawan(termasuk karyawan baru yang sudah ditambahkan pada soal sebelumnya). Lalu hitung jumlah cuti yang sudah diambil pada tahun 2020 dari masing-masing karyawan.
--constraint : Data cuti karyawan baru tidak perlu ditampilkan