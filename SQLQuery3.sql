CREATE DATABASE ArtisProduser

USE ArtisProduser

CREATE TABLE tbArtis(
Kd_Artis varchar (100) primary key,
Nama_Artis varchar (100) not null,
Jenis_kelamin varchar (100) not null,
Bayaran decimal (18,2) not null,
Award int not null,
Negara varchar (100) not null
)

INSERT INTO tbArtis(Kd_Artis, Nama_Artis, Jenis_kelamin, Bayaran, Award, Negara) values
('A001', 'ROWBERT DOWNEY JR', 'PRIA', '2000000000', '2', 'AS'),
('A002', 'ANGELINA JOLIE', 'WANITA', '700000000', '1', 'AS'),
('A003', 'JACKIE CHAN', 'PRIA', '200000000', '7', 'HK'),
('A004', 'JOE TASLIM', 'PRIA', '350000000', '1', 'ID'),
('A005', 'CHELSEA ISLAN', 'WANITA', '300000000', '0', 'ID')

CREATE TABLE tbNegara(
Kd_Negara varchar (100) primary key,
Nama_Negara varchar (100) not null,
)

INSERT INTO tbNegara (Kd_Negara, Nama_Negara) VALUES
('AS', 'AMERIKA SERIKAT'),
('HK', 'HONGKONG'),
('ID', 'INDONESIA'),
('IN', 'INDIA')

CREATE TABLE tbGenre(
Kd_Genre varchar (50) primary key,
Nama_Genre varchar (50) not null,
)

INSERT INTO tbGenre(Kd_Genre, Nama_Genre) VALUES
('G001', 'ACTION'),
('G002', 'HORROR'),
('G003', 'COMEDY'),
('G004', 'DRAMA'),
('G005', 'THRILLER'),
('G006', 'FICTION')

CREATE TABLE tbProduser(
Kd_Produser varchar (50) primary key,
Nama_Produser varchar (50) not null,
International varchar (50) not null
)

INSERT INTO tbProduser(Kd_Produser, Nama_Produser, International) values
('PD01', 'MARVEL','YA'),
('PD02', 'HONGKONG', 'YA'),
('PD03', 'RAPI FILM','TIDAK'),
('PD04', 'PARKIT','TIDAK'),
('PD05', 'PARAMOUNT PICTURE','YA')

CREATE TABLE tbFilm(
Kd_Film varchar (10) primary key,
Nama_Film varchar (55) not null,
Genre varchar (55) not null,
Artis varchar (55) not null,
Produser varchar (55) not null,
Pendapatan decimal (18,2) not null,
Nominasi int not null
)


INSERT INTO tbFilm(Kd_Film, Nama_Film, Genre, Artis, Produser, Pendapatan, Nominasi) values
('F001','IRON MAN','G001','A001','PD01','2000000000','3'),
('F002','IRON MAN 2','G001','A001','PD01','1800000000','2'),
('F003','IRON MAN 3','G001','A001','PD01','1200000000','0'),
('F004','AVENGER : CIVIL WAR','G001','A001','PD01','2000000000','1'),
('F005','SPIDERMAN HOME COMING','G001','A001','PD01','1300000000','0'),
('F006','THE RAID','G001','A004','PD03','800000000','5'),
('F007','FAST & FARIOUS','G001','A004','PD05','830000000','2'),
('F008','HABIBIE DAN AINUN','G004','A005','PD03','670000000','4'),
('F009','POLICE STORY','G001','A003','PD02','700000000','3'),
('F010','POLICE STORY 2','G001','A003','PD02','710000000','1'),
('F011','POLICE STORY 3','G001','A003','PD02','615000000','0'),
('F012','RUSH HOUR','G003','A003', 'PD05','695000000','2'),
('F013','KUNGFU PANDA','G003','A003', 'PD05','923000000','5')

-- NOMER 1 Menampilkan nama film dan nominasi dari nominasi yang paling besar 
SELECT Nama_Film, MAX(Nominasi) AS Nominasi_Terbesar FROM tbFilm
GROUP BY Nama_Film
ORDER BY MAX(Nominasi) DESC

-- NOMER 2 Menampilkan nama film dan nominasi yang paling banyak memperoleh nominasi 
SELECT Nama_Film, (SELECT MAX (Nominasi) FROM tbFilm)
FROM tbFilm 
WHERE Nominasi = (SELECT MAX(Nominasi) FROM tbFilm) 

-- NOMER 3 Menampilkan nama film dan nominasi yang tidak mendapatkan nominasi 
SELECT Nama_Film, Nominasi FROM tbFilm
WHERE Nominasi = 0

-- NOMER 4 Menampilkan nama film dan pendapatan dari yang paling kecil 
SELECT Nama_Film, (SELECT MIN(Pendapatan) FROM tbFilm) 
FROM tbFilm
WHERE Pendapatan= (SELECT MIN(Pendapatan) FROM tbFilm) 

--NOMER 5 Menampilkan nama film dan pendapatan yang terbesar 
SELECT Nama_Film, (SELECT MAX(Pendapatan) FROM tbFilm) 
FROM tbFilm
WHERE Pendapatan= (SELECT MAX(Pendapatan) FROM tbFilm) 

--NOMER 6 Menampilkan nama film yang huruf depannya 'p' 
SELECT Nama_Film FROM tbFilm
WHERE Nama_Film LIKE 'P%'

--NOMER 7 Menampilkan nama film yang huruf terakhir 'y' 
SELECT Nama_Film FROM tbFilm
WHERE Nama_Film LIKE '%y'

-- NOMER 8 Menampilkan nama film yang mengandung huruf 'd' 
SELECT Nama_Film FROM tbFilm
WHERE Nama_Film LIKE '%y'

-- NOMER 9 Menampilkan nama film dengan pendapatan terbesar mengandung huruf 'o'
SELECT Nama_Film,
(SELECT MAX(Pendapatan) FROM tbFilm)
FROM tbFilm
WHERE Pendapatan = (SELECT MAX(Pendapatan) FROM tbFilm) AND Nama_Film LIKE '%o%'

--NOMER 10 Menampilkan nama film dengan pendapatan terkecil mengandung huruf 'o' 
SELECT Nama_Film,
(SELECT MIN(Pendapatan) FROM tbFilm)
FROM tbFilm
WHERE Pendapatan = (SELECT MIN(Pendapatan) FROM tbFilm) AND Nama_Film LIKE '%o%'

--NOMER 11 Menampilkan nama film dan artis 
SELECT tbFilm.Nama_Film, tbArtis.Nama_Artis FROM tbFilm
LEFT JOIN tbArtis ON tbFilm.Artis = tbArtis.Kd_Artis

--NOMER 12 Menampilkan nama film yang artisnya berasal dari negara hongkong
SELECT Nama_Negara, tbFilm.Nama_Film, tbArtis.Nama_Artis FROM tbFilm
LEFT JOIN tbArtis ON tbFilm.Artis = tbArtis.Kd_Artis
LEFT JOIN tbNegara ON tbArtis.Negara = tbNegara.Kd_Negara
WHERE tbNegara.Nama_Negara='HONGKONG'

-- NOMER 13 Menampilkan nama film yang artisnya bukan berasal dari negara yang tidak mengandung huruf 'o'
SELECT Nama_Negara, tbFilm.Nama_Film, tbArtis.Nama_Artis FROM tbFilm
LEFT JOIN tbArtis ON tbFilm.Artis = tbArtis.Kd_Artis
LEFT JOIN tbNegara ON tbArtis.Negara = tbNegara.Kd_Negara
WHERE tbNegara.Nama_Negara NOT LIKE '%o%'

-- NOMER 14 Menampilkan nama artis yang tidak pernah bermain film 
SELECT Nama_Artis FROM tbArtis
LEFT JOIN tbFilm ON tbArtis.Kd_Artis=tbFilm.Artis
WHERE Kd_Film IS NULL

-- NOMER 15 Menampilkan nama artis yang bermain film dengan genre drama 
SELECT Nama_Artis FROM tbArtis
LEFT JOIN tbFilm ON tbArtis.Kd_Artis=tbFilm.Artis
LEFT JOIN tbGenre ON tbFilm.Genre = tbGenre.Kd_Genre
WHERE tbGenre.Nama_Genre='DRAMA'

--NOMER 16 Menampilkan nama artis yang bermain film dengan genre Action
SELECT Nama_Artis FROM tbArtis
LEFT JOIN tbFilm ON tbArtis.Kd_Artis=tbFilm.Artis
LEFT JOIN tbGenre ON tbFilm.Genre = tbGenre.Kd_Genre
WHERE tbGenre.Nama_Genre='ACTION'

--NOMER 17 Menampilkan data negara dengan jumlah filmnya

-- NOMER 18 Menampilkan nama film yang skala internasional 

-- NOMER 19 Menampilkan jumlah film dari masing2 produser 

-- NOMER 20 Menampilkan jumlah pendapatan produser marvel secara keseluruhan 


-- NOMER 4 Menampilkan nama film dan pendapatan dari yang paling kecil 
SELECT Nama_Film, Pendapatan FROM tbFilm
ORDER BY Pendapatan ASC

--NOMER 5 Menampilkan nama film dan pendapatan yang terbesar 
SELECT Nama_Film, Pendapatan
FROM tbFilm
WHERE Pendapatan = (SELECT MAX(Pendapatan) FROM tbFilm)

-- NOMER 6 Menampilkan nama film yang huruf depannya 'p' 
SELECT Nama_Film FROM tbFilm
WHERE Nama_Film LIKE 'P%'

-- NOMER 7 Menampilkan nama film yang huruf terakhir 'y' 
SELECT Nama_Film FROM tbFilm
WHERE Nama_Film LIKE '%y'

-- NOMER 8 Menampilkan nama film yang mengandung huruf 'd' 
SELECT Nama_Film FROM tbFilm
WHERE Nama_Film LIKE '%d%'

-- NOMER 9 Menampilkan nama film dengan pendapatan terbesar mengandung huruf 'o'
SELECT Nama_Film, Pendapatan FROM tbFilm
WHERE Pendapatan = (SELECT MAX(Pendapatan) FROM tbFilm) AND Nama_Film LIKE '%o%'

-- NOMER 10 Menampilkan nama film dengan pendapatan terkecil mengandung huruf 'o' 
SELECT Nama_Film, Pendapatan FROM tbFilm
WHERE Pendapatan = (SELECT MIN(Pendapatan) FROM tbFilm) AND Nama_Film LIKE '%o%'
