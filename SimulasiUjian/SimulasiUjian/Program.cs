﻿using System;

namespace SimulasiUjian
{
    class Program
    {
        static void Main(string[] args)
        {
            bool ulangi = true;
            while (ulangi)
            {
                Console.WriteLine("Pilih Soal 1/2/3/4/5 :");
                int pilih = int.Parse(Console.ReadLine());

                switch (pilih)
                {
                    case 1:
                        soal1();
                        break;
                    //case 2:
                      //  soal2();
                        //break;
                    case 3:
                        soal3();
                        break;
                    case 4:
                        soal4();
                        break;
                    //case 5:
                    //    soal5();
                    //    break;
                    default:
                        break;
                }
                Console.WriteLine("Ulangi (y/n) = ");
                string yesno = Console.ReadLine();
                if (yesno.ToLower() == "n")
                {
                    ulangi = false;

                }
                Console.Clear();
            }
        }
        static void soal1()
        {
            Console.Write("Masukan Kalimat : ");
            string kata = Console.ReadLine();
           
            foreach ( char c in kata)
            {
                Console.Write($"[{c}] ");
            }
        }

        static void soal3()
        {
            Console.Write("Jumlah Keranjang 1 :");
            int keranjang1 = int.Parse(Console.ReadLine());
            Console.Write("Jumlah Keranjang 2 :");
            int keranjang2 = int.Parse(Console.ReadLine());
            Console.Write("Jumlah Keranjang 3 :");
            int keranjang3 = int.Parse(Console.ReadLine());
            int hasil1, hasil2, hasil3 = 0;

            if (keranjang1 == 0)
            {
                hasil1 = keranjang2 + keranjang3;
                Console.Write(hasil1 + "Sisa Buah : "+hasil1);
            } else if (keranjang2 == 0)
            {
                hasil2 = keranjang1 + keranjang3;
                Console.Write("Sisa Buah : "+ hasil2);
            } else if (keranjang3 == 0)
            {
                hasil3 = keranjang2 + keranjang3;
                Console.Write("Sisa Buah : "+hasil3);
            }
        }
        static void soal4()
        {
            Console.Write("Masukan Jumlah Laki Dewasa :  ");
            int laki = int.Parse(Console.ReadLine());
            Console.Write("Masukan Jumlah Wanita Dewasa :  ");
            int wanita = int.Parse(Console.ReadLine());
            Console.Write("Masukan Jumlah Anak-anak :  ");
            int anak = int.Parse(Console.ReadLine());
            Console.Write("Masukan Jumlah Bayi :  ");
            int bayi = int.Parse(Console.ReadLine());
            int hasil, total = 0;

            hasil = (laki * 1) + (wanita * 2) + (anak * 3) + (bayi * 5);
            Console.Write("Hasilnya : " + hasil);
            if (hasil % 3 == 1)
            {
                total = hasil + (wanita * 1);
                Console.Write("Total Seluruhnya : "+total);
            } 
        }
        static void soal7()
        {

        }
    }
}
