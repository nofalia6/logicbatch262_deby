CREATE DATABASE DB_HR

USE DB_HR

CREATE TABLE tb_divisi(
id bigint primary key identity (1,1),
Kd_Divisi varchar (50) not null,
Nama_Divisi varchar (50) not null
)

INSERT INTO tb_divisi (Kd_Divisi, Nama_Divisi) values
('GD', 'Gudang'),
('HRD', 'HRD'),
('KU', 'Keuangan'),
('UM', 'Umum')

CREATE TABLE tb_jabatan(
id bigint primary key identity (1,1),
Kd_Jabatan varchar (50) not null,
Nama_jabatan varchar (50) not null,
Gaji_Pokok decimal (18,4) not null,
Tunjangan_Jabatan decimal (18,4) not null
)

INSERT INTO tb_jabatan (Kd_Jabatan, Nama_Jabatan, Gaji_Pokok, Tunjangan_Jabatan) values
('MGR', 'Manager', '5500000','1500000'),
('OB', 'Office Boy', '1900000','200000'),
('ST', 'Staff', '3000000','750000'),
('WMMGR', 'Wakil Manager', '4000000','1200000')

CREATE TABLE tb_karyawan(
id bigint primary key identity (1,1),
NIP varchar (50) not null,
Nama_Depan varchar (50) not null,
Nama_Belakang varchar (50) not null,
Jenis_Kelamin varchar (50) not null,
Agama varchar (50) not null,
Tempat_Lahir varchar (50) not null,
Tgl_Lahir date not null,
Alamat varchar (50) not null,
Pendidikan varchar (50) not null,
Tgl_Masuk date not null
)

DROP TABLE tb_karyawan

INSERT INTO tb_karyawan (NIP, Nama_Depan, Nama_Belakang, Jenis_Kelamin, Agama, Tempat_Lahir, Tgl_Lahir, Alamat, Pendidikan, Tgl_Masuk) values
('001', 'Hamidi','Samsudi', 'Pria', 'Islam', 'Sukabumi', '1977-04-21', 'Jl. Sudirman No. 12', 'S1 Teknik Mesin', '2015-12-07'),
('003', 'Ghandi','Wamdia', 'Wanita', 'Islam', 'Palu', '1992-01-12', 'Jl. Rambutan No. 22', 'SMA Negeri 02 Palu', '2014-12-01'),
('002', 'Paul', 'Christian', 'Pria', 'Kristen', 'Ambon', '1980-05-27', 'Jl. Veteran No.4', 'S1 Pendidikan Geografi', '2014-01-12')
 
 CREATE TABLE tb_pekerjaan(
id bigint primary key identity (1,1),
NIP varchar (50) not null,
Kd_Jabatan varchar (50) not null,
Kd_Divisi varchar (50) not null,
Tunjangan_Kinerja  decimal (18,4) not null,
Kota_penempatan  varchar (50) not null
)

INSERT INTO tb_pekerjaan (NIP, Kd_Jabatan, Kd_Divisi, Tunjangan_Kinerja, Kota_penempatan) values
('001', 'ST', 'KU', '750000', 'Cianjur'),
('002', 'OB', 'UM', '350000', 'Sukabumi'),
('003', 'MGR', 'HRD', '1500000', 'Sukabumi')

SELECT *FROM tb_divisi
SELECT *FROM tb_jabatan
SELECT *FROM tb_karyawan
SELECT *FROM tb_pekerjaan

-- NOMER 1 Tampilkan nama lengkap, nama jabatan, total tunjangan, yang gaji + tunjangannya dibawah 5juta 
SELECT CONCAT(Nama_Depan,' ', Nama_Belakang) AS Nama_Lengkap, Nama_Jabatan, (Gaji_Pokok+Tunjangan_Jabatan) AS Gaji_Tunjangan FROM tb_karyawan 
LEFT JOIN tb_pekerjaan ON tb_karyawan.NIP = tb_pekerjaan.NIP
LEFT JOIN tb_jabatan ON tb_pekerjaan.Kd_Jabatan = tb_jabatan.Kd_Jabatan
WHERE (Gaji_Pokok+Tunjangan_Jabatan) < 5000000

-- NOMER 2 Tampilkan nama lengkap, jabatan, nama divisi, total gaji, pajak, gaji bersih, yg gendernya pria dan penempatan kerjanya diluar sukabumi

ELECT CONCAT(Nama_Depan, ' ',Nama_Belakang) AS Nama_Lengkap,
tb_jabatan.Nama_Jabatan, tb_divisi.Nama_Divisi, (Gaji_Pokok+Tunjangan_Jabatan+Tunjangan_Kinerja) AS Total_Gaji,
(Gaji_Pokok+Tunjangan_Jabatan+Tunjangan_Kinerja)*0.05 AS Pajak,
(Gaji_Pokok+Tunjangan_Jabatan+Tunjangan_Kinerja) - ((Gaji_Pokok+Tunjangan_Jabatan+Tunjangan_Kinerja)*0.05) AS Gaji_Bersih
FROM tb_karyawan
LEFT JOIN tb_pekerjaan ON  tb_karyawan.NIP = tb_pekerjaan.NIP
RIGHT JOIN tb_jabatan ON tb_jabatan.Kd_Jabatan = tb_pekerjaan.Kd_Jabatan
JOIN tb_divisi ON tb_divisi.Kd_Divisi = tb_pekerjaan.Kd_Divisi
WHERE tb_karyawan.Jenis_Kelamin = 'Pria' AND tb_pekerjaan.Kota_Penempatan != 'Sukabumi' 

-- NOMER 3 Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus (bonus=25% dari total gaji(gaji pokok+tunjangan_jabatan+tunajangan_kinerja * 7) 

SELECT tb_karyawan.NIP, CONCAT(Nama_Depan,' ', Nama_Belakang) AS Nama_Lengkap, tb_jabatan.Nama_jabatan, tb_divisi.Nama_Divisi, 
(Gaji_Pokok+Tunjangan_Jabatan+Tunjangan_Kinerja)*7*0.25 AS Bonus FROM tb_karyawan
LEFT JOIN tb_pekerjaan ON tb_karyawan.NIP = tb_pekerjaan.NIP
RIGHT JOIN tb_jabatan ON tb_jabatan.Kd_Jabatan = tb_pekerjaan.Kd_Jabatan
JOIN tb_divisi ON tb_divisi.Kd_Divisi=tb_pekerjaan.Kd_Divisi

-- NOMER 4 Tampilkan nama lengkap, ttal gaji, infak(5%*total gaji) yang mempunyai jabatan MGR 
SELECT CONCAT(Nama_Depan,' ', Nama_Belakang) AS Nama_Lengkap,
tb_jabatan.Nama_Jabatan, tb_divisi.Nama_Divisi, (Gaji_Pokok+Tunjangan_Jabatan+Tunjangan_Kinerja) AS Total_Gaji,
(Gaji_Pokok+Tunjangan_Jabatan+Tunjangan_Kinerja)*0.05 AS Infak FROM tb_karyawan
LEFT JOIN tb_pekerjaan ON tb_karyawan.NIP = tb_pekerjaan.NIP
RIGHT JOIN tb_jabatan ON tb_jabatan.Kd_Jabatan = tb_pekerjaan.Kd_Jabatan
LEFT JOIN tb_divisi ON tb_divisi.Kd_Divisi=tb_pekerjaan.Kd_Divisi
WHERE tb_jabatan.Nama_jabatan = 'Manager'

-- NOMER 5 Tampilkan nama lengkap, nama jabatan, pendidikan terakhir, tunjangan pendidikan(2jt), dan total gaji(gapok+tjabatan+tpendidikan) dimana pendidikan akhirnya adalah S1
SELECT CONCAT(Nama_Depan,' ', Nama_Belakang) AS Nama_Lengkap,
tb_jabatan.Nama_jabatan, tb_karyawan.Pendidikan, 2000000 AS Tunjangan_Pendidikan,
(Gaji_Pokok+Tunjangan_Jabatan+Tunjangan_Kinerja+2000000) AS Total_Gaji
FROM tb_karyawan
LEFT JOIN tb_pekerjaan ON tb_karyawan.NIP = tb_pekerjaan.NIP
LEFT JOIN tb_jabatan ON tb_jabatan.Kd_Jabatan = tb_pekerjaan.Kd_Jabatan

-- NOMER 6  Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus
SELECT tb_karyawan.NIP, CONCAT(Nama_Depan, ' ', Nama_Belakang) AS Nama_Lengkap,
tb_jabatan.Nama_Jabatan, tb_karyawan.Pendidikan,
CASE tb_pekerjaan.Kode_Jabatan 
	WHEN 'ST' then ((Gaji_Pokok+Tunjangan_Jabatan+Tunjangan_Kinerja)*5)*0.25
	WHEN 'MGR' then ((Gaji_Pokok+Tunjangan_Jabatan+Tunjangan_Kinerja)*7)*0.25
	WHEN 'OB' then ((Gaji_Pokok+Tunjangan_Jabatan+Tunjangan_Kinerja)*2)*0.25
END AS BONUS
FROM tb_karyawan 
LEFT JOIN tb_pekerjaan ON tb_karyawan.NIP = tb_pekerjaan.NIP
LEFT JOIN tb_jabatan ON tb_jabatan.Kd_Jabatan = tb_pekerjaan.Kd_Jabatan
LEFT JOIN tb_divisi ON tb_divisi.Kd_Divisi=tb_pekerjaan.Kd_Divisi

-- NOMER 7 Buatlah kolom nip pada table karyawan sebagai kolom unique
 CREATE UNIQUE INDEX idx_nip
 ON tb_karyawan(NIP)

 -- NOMER 8 buatlah kolom nip pada table employee sebagai index
CREATE INDEX idx2_nip
ON tb_karyawan (NIP)

-- NOMER 9 Tampilkan nama lengkap dan nama belakangnya  diubah menjadi huruf capital dengan last namanya di awali dengan huruf W 
SELECT UPPER(CONCAT(Nama_Depan, ' ', Nama_Belakang)) AS Nama_Lengkap FROM tb_karyawan 
WHERE Nama_Belakang LIKE ='w%'

