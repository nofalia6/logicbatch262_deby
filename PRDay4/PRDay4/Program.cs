﻿using System;

namespace PRDay4
{
    class Program
    {
        static void Main(string[] args)
        {
    
                bool ulangi = true;
                while (ulangi)
                {
                    Console.WriteLine("Pilih Soal 1/2/3/4/5 :");
                    int soal = int.Parse(Console.ReadLine());

                    Console.WriteLine("====SOAL ARRAY====");

                    switch (soal)
                    {
                        case 1:
                            soal1();
                            break;
                        case 2:
                            soal2();
                            break;
                        //case 3:
                        //    soal3();
                        //    break;
                        //case 4:
                        //    soal4();
                        //    break;
                        //case 5:
                        //    soal5();
                        //    break;
                        //default:
                        //    break;
                    }

                    Console.WriteLine("Ulangi (y/n) = ");
                    string yesno = Console.ReadLine();
                    if (yesno.ToLower() == "n")
                    {
                        ulangi = false;

                    }
                    Console.Clear();
                }

        }
        static void soal1()
        {
            string abc = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            char[] arrAbc = abc.ToCharArray();
            Console.WriteLine("Masukan Kata Sandi/Password : ");
            string sandi = Console.ReadLine();
            Console.WriteLine("Masukan Rotasi : ");
            int rotasi = int.Parse(Console.ReadLine());
            string encryptKalimat = "";
            for (int i = 0; i < sandi.Length; i++)
            {
                char kata = sandi[i];
                int idx = Array.IndexOf(arrAbc, kata);
                if (idx != -1)
                {
                    encryptKalimat += arrAbc[idx + rotasi];
                }
                else
                {
                    encryptKalimat += sandi[i];
                }
            }
            Console.WriteLine($"Hasilnya : {encryptKalimat}");
        }

        static void soal2()
        {
            Console.Write("Masukkan Bilangan : ");
            int input = int.Parse(Console.ReadLine());
            int angka = 0;
            for (int i = 0; i < (input * 3) - 1; i++)
            {
                Console.Write(angka + " ");
                angka++;
                if (angka % input == 0)
                {
                    Console.WriteLine();

                }
            }
            Console.Write(angka + " ");
        }
        static void soal3()
        {

        }
        static void soal5()
        {

        }
    }
}
