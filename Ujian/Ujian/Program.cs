﻿using System;

namespace Ujian
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.Write("Pilih Soal 1 - 10 :");
            int pilih = int.Parse(Console.ReadLine());

            switch (pilih)
            {
                case 1:
                    Class1 s1 = new Class1();
                    s1.soal1();
                    break;
                /*case 2:
                    Class2 s2 = new Class2();
                    s2.soal2();
                    break;
                case 3:
                    Class3 s3 = new Class3();
                    s3.soal3();
                    break;*/
                case 4:
                    Class4 s4 = new Class4();
                    s4.soal4();
                    break;
                case 5:
                    Class5 s5 = new Class5();
                    s5.soal5();
                    break;
                case 6:
                    Class6 s6 = new Class6();
                    s6.soal6();
                    break;
                case 7:
                    Class7 s7 = new Class7();
                    s7.soal7();
                    break;
                case 8:
                    Class8 s8 = new Class8();
                    s8.soal8();
                    break;
                case 9:
                    Class9 s9 = new Class9();
                    s9.soal9();
                    break;
                case 10:
                    Class10 s10 = new Class10();
                    s10.soal10();
                    break;
            }
        }
       
    }
}
