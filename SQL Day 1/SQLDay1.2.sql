CREATE DATABASE DBKampus

USE DBKampus
CREATE TABLE Mahasiswa(
id int primary key identity(1,1),
NIM varchar(3) not null,
Nama varchar (10) not null,
Kelamin varchar (1) not null,
Alamat varchar (80) not null
)
SELECT *FROM Mahasiswa
INSERT INTO Mahasiswa (NIM, Nama, Kelamin, Alamat) values
('101','Arif','L', 'Jl. Kenangan'),
('102','Budi','L', 'Jl. Jombang'),
('103','Wati','P', 'Jl. Surabaya'),
('104','Ika','P', 'Jl. Jombang'),
('105','Tono','L', 'Jl. Jakarta'),
('106','Iwan','L', 'Jl. Bandung'),
('107','Sari','P', 'Jl. Malang'


CREATE TABLE ambil_mk (
id int primary key identity(1,1),
NIM varchar(3) not null,
kode_mk varchar (10) not null,
)

INSERT INTO ambil_mk (NIM, kode_mk) values
('101','PTI447'),
('103','TIK333'),
('104','PTI333'),
('104','PTI777'),
('111','PTI123'),
('123','PTI999')


CREATE TABLE MataKuliah (
id int primary key identity(1,1),
kode_mk varchar (10) not null,
Nama_mk varchar (30) not null,
sks int  not null,
semester int  not null,
)

INSERT INTO MataKuliah(kode_mk, Nama_mk, sks, semester) values
('PTI447','Praktikum Basis Data', 1,3),
('TIK342','Praktikum Basis Data', 1,3),
('PTI333', 'Basis Data Terdistribusi', 3,5),
('TIK123', 'Jaringan Komputer', 2,5),
('TIK333', 'Sistem Operasi',3,5),
('PTI123', 'Grafika Multimedia', 3, 5),
('PTI777', 'Sistem Informasi',2, 3)

DELETE FROM MataKuliah
SELECT *FROM Mahasiswa
SELECT *FROM ambil_mk
SELECT *FROM MataKuliah

-- NOMER 1 Tampilkan nama mahasiswa dan matakuliah yang diambil
SELECT  Nama, Nama_mk from Mahasiswa JOIN ambil_mk on Mahasiswa.NIM=ambil_mk.NIM
JOIN MataKuliah on MataKuliah.kode_mk=ambil_mk.kode_mk

-- NOMER 2 Tampilkan data mahasiswa yang tidak mengambil matakuliah
SELECT * FROM Mahasiswa LEFT JOIN ambil_mk on Mahasiswa.NIM=ambil_mk.NIM
WHERE kode_mk is null

-- NOMER 3 Kelompokan data mahasiswa yang tidak mengambil matakuliah berdasarkan jenis kelaminnya, kemudian hitung banyaknya
SELECT COUNT(Kelamin) AS Jumlah_Mahasiswa,Kelamin FROM Mahasiswa LEFT JOIN ambil_mk on Mahasiswa.NIM =ambil_mk.NIM
WHERE kode_mk is null GROUP BY Kelamin

-- NOMER 4 Dapatkan nim dan nama mahasiswa yang mengambil matakuliah beserta kode_mk dan nama_mk yang diambilnya 
SELECT Mahasiswa.NIM, Nama, ambil_mk.kode_mk, Nama_mk FROM Mahasiswa JOIN ambil_mk 
on Mahasiswa.NIM=ambil_mk.NIM JOIN MataKuliah ON MataKuliah.kode_mk=ambil_mk.kode_mk 

-- NOMER 5 Dapatkan nim, nama, dan total sks yang diambil oleh mahasiswa, Dimana total sksnya lebih dari 4 dan kurang dari 10. 
SELECT Nama, x.NIM, SUM(sks) AS Total_SKS  FROM ambil_mk AS x
JOIN Mahasiswa AS y ON y.NIM=x.NIM join MataKuliah AS z ON x.kode_mk=z.kode_mk 
GROUP BY Nama,x.NIM HAVING SUM(sks)>4 AND SUM(sks)<10

-- NOMER 6 Dapatkan data matakuliah yang tidak diambil oleh mahasiswa terdaftar (mahasiswa yang terdaftar adalah mahasiswa yang tercatat di tabel mahasiswa).
SELECT Nama_mk, MataKuliah.kode_mk, sks, semester FROM Mahasiswa JOIN ambil_mk ON Mahasiswa.NIM=ambil_mk.NIM
RIGHT JOIN MataKuliah ON ambil_mk.kode_mk=MataKuliah.kode_mk WHERE Mahasiswa.Nama is null