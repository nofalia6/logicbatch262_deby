CREATE DATABASE Penerbit

USE Penerbit
CREATE TABLE tbPengarang(
id int primary key identity (1,1),
Kd_Pengarang varchar (7) not null,
Nama varchar (30) not null,
Alamat varchar (80) not null,
Kota varchar (15) not null,
Kelamin varchar (1) not null
)
INSERT INTO tbPengarang (Kd_Pengarang, Nama, Alamat,Kota,Kelamin) values
('P0001','Ashadi','Jl. Beo 25', 'Yogya', 'P'),
('P0002','Rian','Jl. Solo 123', 'Yogya', 'P'),
('P0003','Suwadi','Jl. Solo 123', 'Bandung', 'P'),
('P0004','Siti','Jl. Durian 15', 'Solo', 'W'),
('P0005','Amir','Jl. Gajah 33', 'Kudus', 'P'),
('P0006','Suparman','Jl. Harimau 25', 'Jakarta', 'P'),
('P0007','Jaja','Jl. Singa 7', 'Bandung', 'P'),
('P0008','Saman','Jl. Naga 12', 'Yogya', 'P'),
('P0009','Anwar','Jl. Tidar 6A', 'Magelang', 'P'),
('P0010','Fatmawati','Jl. Renajana 4', 'Bogor', 'W')

SELECT * From tbPengarang

CREATE TABLE tblGaji(
id int primary key identity (1,1), 
Kd_Pengarang varchar (7) not null,
Nama varchar (30) not null,
Gaji decimal (18,4) not null
)

INSERT INTO tblGaji (Kd_Pengarang, Nama, Gaji) values
('P0002','Rian','600000'),
('P0005','Amir','700000'),
('P0004','Siti','500000'),
('P0003','Suwadi','1000000'), 
('P0010','Fatmawati','600000'),
('P0008','Saman','750000')

SELECT *FROM tbPengarang
SELECT *FROM tblGaji

--NOMER 1 Hitung dan tampilkan jumlah pengarang dari table tblPengarang
SELECT Nama, Count(*) jml_pengarang FROM tbPengarang GROUP BY Nama
SELECT Count(Nama) FROM tbPengarang

--NOMER 2 Hitunglah berapa jumlah Pengarang Wanita dan Pria
SELECT 

--NOMER 3 Tampilkan record kota dan jumlah kotanya dari table tblPengarang. 
SELECT Kota, Count(*) AS jml_Kota FROM tbPengarang GROUP BY Kota

--NOMER 4 Tampilkan record kota diatas 1 kota dari table tblPengarang.
SELECT Kota, Count(*) as jml_Kota FROM tbPengarang Where Kota

--NOMER 5 Tampilkan Kd_Pengarang yang terbesar dan terkecil dari table tblPengarang
SELECT * FROM tbPengarang ORDER BY Kd_Pengarang DESC;

--NOMER 6 Tampilkan gaji tertinggi dan terendah
SELECT MAX(Gaji) AS gaji_tertinggi, MIN(Gaji) AS gaji_terendah
FROM tblGaji; 

SELECT * FROM tblGaji ORDER BY Gaji DESC;

--NOMER 7 Tampilkan gaji diatas 600.000
SELECT *FROM tblGaji Where Gaji > 600000;

--NOMER 8 Tampilkan jumlah gaji
SELECT SUM (Gaji) as jumlahGaji From tblGaji;

-- NOMER 9 Tampilkan jumlah gaji berdasarkan Kota
SELECT Kota, Count(*), SUM (Gaji) FROM tbPengarang as pengarang join tblGaji as Gaji on pengarang.Kd_Pengarang = Gaji.Kd_Pengarang

-- NOMER 10 Tampilkan seluruh record pengarang antara P0001-P0006 dari tabel pengarang
SELECT * FROM tbPengarang WHERE Kd_Pengarang between 'P0001' and 'P0006'

-- NOMER 11 Tampilkan seluruh data yogya, solo, dan magelang dari tabel pengarang
SELECT * FROM tbPengarang WHERE Kota='yogya' or Kota='solo' or Kota='Magelang'

-- NOMER 12 Tampilkan seluruh data yang bukan yogya dari tabel pengarang
SELECT * FROM tbPengarang WHERE Kota != 'yogya' 
-- NOMER 13 Tampilkan seluruh data pengarang yang nama (dapat digabungkan atau terpisah):
-- a:
SELECT * FROM tbPengarang WHERE Nama LIKE 'a%'
-- b:
SELECT * FROM tbPengarang WHERE Nama LIKE '%i'

--c:
SELECT *FROM tbPengarang WHERE Nama LIKE '__a%'
--d:
SELECT *FROM tbPengarang WHERE Nama NOT LIKE '%n'

-- NOMER 14 Tampilkan seluruh data table tblPengarang dan tblGaji dengan Kd_Pengarang yang sama

SELECT *FROM tbPengarang join tblGaji on tbPengarang.Kd_Pengarang=tblGaji.Kd_Pengarang

-- NOMER 15

SELECT * FROM tbPengarang join tblGaji on tbPengarang.Kd_Pengarang=tblGaji.Kd_Pengarang WHERE Gaji <1000000

-- NOMER 16
ALTER TABLE tbPengarang ALTER COLUMN Kelamin varchar (10)

--NOMER 17

ALTER TABLE tbPengarang add gelar varchar(12)

-- NOMER 18
Update tbPengarang set Alamat = 'Jl. Cendrawasih 65', Kota = 'Pekanbaru' WHERE Nama ='Rian'

--NOMER 19 


-- NOMER 7 Tampilkan gaji diatas 600.000.
SELECT * FROM tblGaji WHERE Gaji > 600000

-- NOMER 8 Tampilkan jumlah gaji.
SELECT SUM(Gaji) AS jml_gaji FROM tblGaji

-- NOMER 9 Tampilkan jumlah gaji berdasarkan Kota
SELECT Kota,  Count(Kota) AS jml_kota, SUM(Gaji) AS jml_gaji
FROM tbPengarang as Pengarang left join tblGaji AS Gaji on Pengarang.Kd_Pengarang=Gaji.Kd_Pengarang GROUP BY Kota

DELETE FROM tbPengarang