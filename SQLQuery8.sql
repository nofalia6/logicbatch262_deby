CREATE DATABASE DBKaryawan 

USE DBKaryawan

CREATE TABLE Karyawan(
id bigint primary key identity (1,1),
Nomor_Induk varchar (7) NOT NULL,
Nama varchar (30) not null,
Alamat text not null,
Tgl_lahir date not null,
Tgl_masuk date not null,
)

INSERT INTO Karyawan (Nomor_Induk, Nama, Alamat, Tgl_lahir, Tgl_masuk) Values
('IP06001', 'Agus','Jln. Gajah Mada 115A, Jakarta Pusat', '8/1/70','7/7/06'),
('IP06002', 'Amin','Jln. Bungur Sari V No.178, Bandung', '5/3/77','7/6/06'),
('IP06003', 'Yusuf','Jln. Yosodpuro 15, Surabaya', '8/9/73','7/8/06'),
('IP07004', 'Alyssa','Jln. Cendana No. 6, Bandung', '2/14/83','1/5/07'),
('IP07005', 'Maulana','Jln. Ampera Raya No.1', '10/10/85','2/5/07'),
('IP07006', 'Afika','Jln. Pejaten Barat No.6A', '3/9/87','6/9/07'),
('IP07007', 'James','Jln. Padjadjaran No.111, Bandung', '5/19/88','6/9/06'),
('IP09008', 'Octavanus','Jln. Gajah Mada 101, Semarang', '10/7/88','8/8/08'),
('IP09009', 'Nugroho','Jln. Duren Tiga 196, Jakarta Selatan', '1/20/88','11/11/08'),
('IP09010', 'Raisa','Jln. Nangka Jakarta Selatan', '12/29/89', '2/9/09')

CREATE TABLE Cuti_Karyawan(
id bigint primary key identity (1,1),
Nomor_Induk varchar (7) NOT NULL,
Tgl_mulai date NOT NULL,
Lama_cuti int  NOT NULL,
Keterangan text NOT NULL,
)

INSERT INTO Cuti_Karyawan (Nomor_Induk, Tgl_mulai, Lama_cuti, Keterangan) values
('IP06001', '2/1/12', 3, 'Acara Keluar'),
('IP06001', '2/13/12', 4, 'Anak Sakit'),
('IP07007', '2/15/12', 2, 'Nenek Sakit'),
('IP06003', '2/17/12', 1, 'Mendaftar Sekolah Anak'),
('IP06006', '2/20/12', 5, 'Menikah'),
('IP07004', '2/27/12', 1, 'Imunisasi Anak')

-- NOMER 1 Menampilkan 3 karyawan yang pertama kali masuk
SELECT top(3) Nama, Tgl_masuk FROM Karyawan
ORDER BY Tgl_masuk ASC

-- NOMER 2 Menampilkan daftar karyawan yang saat ini sedang cuti. Daftar berisi nomor_induk, nama, tanggal_mulai, lama_cuti dan keterangan
SELECT Karyawan.Nomor_Induk, Nama, DATEADD(day, Lama_Cuti, Tgl_mulai) AS Tanggal_Cuti, Keterangan FROM Karyawan
RIGHT JOIN Cuti_Karyawan ON Cuti_Karyawan.Nomor_Induk=Karyawan.Nomor_Induk
WHERE DATEADD(day, Lama_Cuti, Tgl_mulai)>'2/16/12' AND Tgl_mulai < '2/16/12'

-- NOMER 3 Menampilkan daftar karyawan yang sudah cuti lebih dari satu hari. Daftar berisi no_induk, nama, jumlah (berapa hari cuti)
SELECT Karyawan.Nomor_Induk, Nama, SUM(Lama_Cuti) AS Jum_Cuti FROM Karyawan
JOIN Cuti_Karyawan ON Cuti_Karyawan.Nomor_Induk = Karyawan.Nomor_Induk
WHERE Lama_cuti IS NOT NULL AND Lama_cuti > 1
GROUP BY Karyawan.Nomor_Induk, Karyawan.Nama


-- NOMER 4 Menampilkan sisa cuti tiap karyawan tahun ini, jika di ketahui jatah cuti setiap karyawan tahun ini adalah 12. Daftar berisi no_induk, nama, sisa_cuti

SELECT Karyawan.Nomor_Induk, Nama, 12-SUM(ISNULL(Lama_Cuti, 0)) AS Sisa_Cuti FROM Karyawan
LEFT JOIN Cuti_Karyawan ON Cuti_Karyawan.Nomor_Induk=Karyawan.Nomor_Induk
GROUP BY Karyawan.Nomor_Induk, Karyawan.Nama